
This project is a mockup of a bank blog. See live site here - https://bank-blog.netlify.app/

## Built with
* React & bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
* Uses the [Westpac GEL Design System](https://github.com/WestpacGEL/GEL)
* [Styled-components](https://styled-components.com/) for styling
* [Axios](https://github.com/axios/axios) as an HTTP client
* [react-query](https://github.com/tannerlinsley/react-query) hooks for fetching & caching
* [React Router](https://reactrouter.com/) for client-side routing
