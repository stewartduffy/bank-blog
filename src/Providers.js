import React from 'react'
import { GEL, useBrand } from '@westpac/core'
import { ThemeProvider } from 'styled-components'
import brand from '@westpac/wbc'
import { BrowserRouter } from 'react-router-dom'

const StyledComponentsProvider = ({ children }) => {
  const { COLORS } = useBrand()

  const theme = {
    colors: COLORS,
    fonts: {
      WestpacHelpaBold:
        "'WestpacHelpaBold', 'Helvetica Neue', Helvetica, Arial, sans-serif",
    },
  }

  return <ThemeProvider theme={theme}>{children}</ThemeProvider>
}

const Providers = ({ children }) => {
  return (
    <BrowserRouter>
      <GEL brand={brand}>
        <StyledComponentsProvider>{children}</StyledComponentsProvider>
      </GEL>
    </BrowserRouter>
  )
}

export default Providers
