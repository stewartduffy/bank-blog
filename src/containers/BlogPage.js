import React, { Fragment } from 'react'
import styled from 'styled-components'
import { useQuery } from 'react-query'
import { Switch, Route } from 'react-router-dom'
import { getPostsWithComments } from '../api'
import { Posts, Post } from '../components/'
import { LoadingSkeleton } from '../components'
import businessCoverImage from '../assets/images/business-cover-image.jpg'

function BlogPage() {
  const { isSuccess, data: posts = [] } = useQuery(
    'getPostsWithComments',
    () => getPostsWithComments(),
    {
      refetchOnWindowFocus: false,
    }
  )

  return (
    <BlogPageWrapper>
      <BlogPageHeader>
        <h1>REDnews</h1>
      </BlogPageHeader>
      <LoadingSkeleton ready={isSuccess}>
        <Switch>
          <Route path="/:postId">
            <Post posts={posts} isDetailView />
          </Route>
          <Route>
            <Fragment>
              <img src={businessCoverImage} alt="business meeting" />
              <Posts isSuccess={isSuccess} posts={posts} />
            </Fragment>
          </Route>
        </Switch>
      </LoadingSkeleton>
    </BlogPageWrapper>
  )
}

const BlogPageWrapper = styled.div`
  border: solid 20px ${(props) => props.theme.colors.primary};
  border-top: 0;

  & > img {
    display: none;
    @media (min-width: 992px) {
      display: block;
      padding: 0;
      width: 100%;
      margin: 0;
    }
  }
`

const BlogPageHeader = styled.div`
  background-color: ${(props) => props.theme.colors.primary};

  && h1 {
    font-family: ${(props) => props.theme.fonts.WestpacHelpaBold};
    font-size: 32px;
    margin: 0;
    color: #fff !important;
  }
`

export default BlogPage
