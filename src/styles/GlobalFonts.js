import { createGlobalStyle } from 'styled-components'
import WestpacHelpaBoldWoff from '../assets/fonts/Westpac-Helpa-Bold.woff'
import WestpacHelpaBoldEot from '../assets/fonts/Westpac-Helpa-Bold.eot'

const GlobalFonts = createGlobalStyle`
  @font-face {
    font-family: 'WestpacHelpaBold';
    src: local('WestpacHelpaBold'), local('WestpacHelpaBold'),
    url(${`${WestpacHelpaBoldEot}?#iefix`}) format('embedded-opentype'),
    url(${WestpacHelpaBoldWoff}) format('woff');
    font-weight: 400;
    font-style: normal;
  }
`

export default GlobalFonts
