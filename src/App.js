import React from 'react'
import styled from 'styled-components'
import { Body } from '@westpac/body'
import Providers from './Providers'
import { BlogPage } from './containers/'
import { PageHeader } from './components'
import GlobalFonts from './styles/GlobalFonts'

function App() {
  return (
    <Providers>
      <GlobalFonts />
      <Body>
        <AppLayout>
          <PageHeader />
          <BlogPage />
        </AppLayout>
      </Body>
    </Providers>
  )
}

const AppLayout = styled.div`
  @media (min-width: 992px) {
    width: 960px;
    margin: 0 auto;
  }
`

export default App
