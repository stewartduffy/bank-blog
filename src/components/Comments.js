import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Comment from './Comment'

function Comments({ comments, isDetailView }) {
  const numberOfComments = comments.length

  return (
    <div>
      <CommentsTitle>{numberOfComments} Comments</CommentsTitle>
      {isDetailView &&
        comments.map((comment) => {
          return <Comment key={`comment-${comment.id}`} comment={comment} />
        })}
    </div>
  )
}

Comments.propTypes = {
  comments: PropTypes.array,
  isDetailView: PropTypes.bool,
}

Comments.defaultProps = {
  comments: [],
  isDetailView: false,
}

const CommentsTitle = styled.h4`
  font-size: 16px;
  font-weight: 300;
  margin: 0;
  color: ${(props) => props.theme.colors.primary};
`

export default Comments
