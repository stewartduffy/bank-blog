import React from 'react'
import ReactPlaceholder from 'react-placeholder'
import 'react-placeholder/lib/reactPlaceholder.css'

function LoadingSkeleton({ ready, children, ...rest }) {
  return (
    <ReactPlaceholder
      type="text"
      color="#E0E0E0"
      showLoadingAnimation={true}
      rows={100}
      ready={ready}
      {...rest}
    >
      {children}
    </ReactPlaceholder>
  )
}

export default LoadingSkeleton
