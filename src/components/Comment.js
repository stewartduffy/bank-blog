import React from 'react'
import useTextWithNewLines from '../hooks/useTextWithNewLines'

function Comment({ comment }) {
  const { body, name } = comment
  const { TextWithNewLines: CommentBody } = useTextWithNewLines(body)

  return (
    <div>
      <p>
        <em>
          <CommentBody />
        </em>
      </p>
      <small>
        <a href="#comment">{name}</a>
      </small>
    </div>
  )
}

export default Comment
