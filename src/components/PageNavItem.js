import React from 'react'
import styled from 'styled-components'

function PageNavItem({ text, url }) {
  return (
    <PageNavItemWrapper>
      <a href={url}>{text}</a>
    </PageNavItemWrapper>
  )
}

const PageNavItemWrapper = styled.li`
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  list-style: none;
  padding: 0;
  margin: 0;

  & a {
    display: block;
    font-family: ${(props) => props.theme.fonts.WestpacHelpaBold};
    color: ${(props) => props.theme.colors.heading};
    text-decoration: none;

    &:hover {
      color: ${(props) => props.theme.colors.focus};
      text-decoration: none;
    }
  }
`

export default PageNavItem
