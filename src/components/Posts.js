import React from 'react'
import Post from './Post'
import styled from 'styled-components'
import { Well } from '@westpac/well'

function Posts({ posts }) {
  return (
    <StyledWell>
      <PostsWrapper>
        {posts.map((post) => {
          return <Post key={`post-${post.id}`} post={post} />
        })}
      </PostsWrapper>
    </StyledWell>
  )
}

const StyledWell = styled(Well)`
  background-color: #f4f3f0;
`

const PostsWrapper = styled.div`
  @media (min-width: 576px) {
    display: grid;
    grid-gap: 10px;
    grid-template-columns: repeat(2, minmax(180px, 1fr));
  }

  @media (min-width: 768px) {
    grid-template-columns: repeat(3, minmax(180px, 1fr));
  }
`

export default Posts
