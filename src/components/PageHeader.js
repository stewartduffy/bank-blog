import React from 'react'
import PageNav from './PageNav'

function PageHeader() {
  return (
    <div>
      <PageNav />
    </div>
  )
}

export default PageHeader
