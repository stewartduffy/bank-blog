import React from 'react'
import { WBCLogo } from '@westpac/symbol'
import styled from 'styled-components'
import PageNavItem from './PageNavItem'

const PAGE_NAV_ITEMS = [
  {
    text: 'Personal',
    url: '/',
  },
  {
    text: 'Business',
    url: '/',
  },
  {
    text: 'Agribusiness',
    url: '/',
  },
  {
    text: 'Agribusiness',
    url: '/',
  },
  {
    text: 'Institutional',
    url: '/',
  },
  {
    text: 'Who We Are',
    url: '/',
  },
  {
    text: 'Growing NZ',
    url: '/',
  },
]

function PageNav() {
  return (
    <PageNavWrapper>
      <a href="/">
        <StyledWBCLogo />
      </a>
      <PageNavItems>
        {PAGE_NAV_ITEMS.map(({ text, url }, i) => {
          return <PageNavItem key={`PageNavItem=${i}`} text={text} url={url} />
        })}
      </PageNavItems>
    </PageNavWrapper>
  )
}

const PageNavWrapper = styled.div`
  display: flex;
`

const StyledWBCLogo = styled(WBCLogo).attrs(() => ({ width: 80 }))`
  margin-bottom: 10px;
  margin-right: 30px;
  margin-top: 10px;
`

const PageNavItems = styled.ul`
  /*
   * If this was production code we would render a mobile menu for smaller
   * devices but for this example we just hide the menu.
   * Do not do this for production!
   */
  display: none;

  @media (min-width: 992px) {
    flex-grow: 1;
    display: grid;
    padding: 0;
    margin: 0;
    grid-gap: 10px;
    grid-template-columns: repeat(7, auto);
  }
`

export default PageNav
