import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { useTextWithNewLines } from '../hooks/'
import { truncate } from '../utils'
import Comments from './Comments'
import { Link, useParams } from 'react-router-dom'

function Post({ post, posts, isDetailView }) {
  const { postId } = useParams()

  const postData = isDetailView
    ? posts.find((post) => post.id === parseInt(postId, 10))
    : post

  const { body, title, comments, postImage, id } = postData
  const postBody = isDetailView ? body : truncate(body, 60)

  const { TextWithNewLines: PostContent } = useTextWithNewLines(postBody)

  return (
    <PostWrapper>
      <img src={postImage} alt="Something about business" />
      <PostDetails>
        <PostTitle>
          <Link to={`/${id}`}>{title}</Link>
        </PostTitle>
        <p>
          <PostContent />
        </p>
        {comments && (
          <Comments isDetailView={isDetailView} comments={comments} />
        )}
      </PostDetails>
    </PostWrapper>
  )
}

Post.propTypes = {
  post: PropTypes.object,
  posts: PropTypes.array,
  isDetailView: PropTypes.bool,
}

Post.defaultProps = {
  post: {},
  posts: [],
  isDetailView: false,
}

const PostWrapper = styled.div`
  background: #fff;

  & img {
    min-height: 1px;
    object-fit: contain;
    object-position: left;
    max-width: 100%;
    height: auto;
  }
`

const PostDetails = styled.div`
  padding: 24px;
`

const PostTitle = styled.h3`
  font-size: 18px;
  font-weight: 300;
  text-decoration: none;
  line-height: 1.1;
  margin: 0;
  margin-bottom: 18px;
  text-transform: capitalize;

  :before {
    content: '> ';
    font-size: 20px;
    font-weight: 400;
    color: ${(props) => props.theme.colors.primary};
  }

  & a {
    color: ${(props) => props.theme.colors.heading};
    text-decoration: none;
  }
`

export default Post
