import React, { Fragment } from 'react'

const NEWLINE_STRING = '\n'

function useTextWithNewLines(text) {
  if (typeof text !== 'string') {
    return null
  }

  const textArray = text.split(NEWLINE_STRING)

  const TextWithNewLines = () =>
    textArray.map((line, i) => (
      <Fragment key={i}>
        {line}
        <br />
      </Fragment>
    ))

  return {
    TextWithNewLines,
  }
}

export default useTextWithNewLines
