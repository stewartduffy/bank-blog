import axios from 'axios'
import { getRandomImage } from './utils'

const instance = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com/',
})

export async function getPosts() {
  try {
    const { data } = await instance.get('/posts')
    return data
  } catch (error) {
    console.error(error)
  }
}

export async function getComments() {
  try {
    const { data } = await instance.get('/comments')
    return data
  } catch (error) {
    console.error(error)
  }
}

export async function getPostsWithComments() {
  try {
    const [posts, comments] = await Promise.all([getPosts(), getComments()])
    return processResponse({ posts, comments })
  } catch (error) {
    console.error(error)
  }
}

// Combine Posts & Comments into 1 array for easy consumption
const processResponse = ({ posts = [], comments }) => {
  return posts.map((post) => {
    return {
      ...post,
      postImage: getRandomImage(),
      comments: comments.filter((comment) => comment.postId === post.id),
    }
  })
}
