const truncate = (string, length) => {
  if (string.length > length) {
    return string.substr(0, length) + '\u2026'
  } else {
    return string
  }
}

export default truncate
