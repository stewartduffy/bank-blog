const images = new Array(75).fill().map((d, i) => {
  return `red-news-post-${i + 1}.jpg`
})

const getRandomImage = () => {
  const imageName = images[Math.floor(Math.random() * images.length)]
  return `/cdn/posts/${imageName}`
}

export default getRandomImage
